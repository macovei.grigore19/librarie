CREATE TABLE utilisateurs (
 id INT PRIMARY KEY,
 nom VARCHAR(100),
 prenom VARCHAR(100),
 email VARCHAR(100)
);
CREATE TABLE problemes (
 id INT PRIMARY KEY,
 apparition DATE,
 probleme VARCHAR(200),
 solution VARCHAR(200)
);
CREATE TABLE problemes_utilisateurs (
 utilisateurs_id INT NOT NULL,
 problemes_id INT,
 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id),
 FOREIGN KEY (problemes_id) REFERENCES problemes (id)
);
CREATE TABLE livres (
 id INT PRIMARY KEY,
 titre VARCHAR(100),
 auteur VARCHAR(100),
 categorie VARCHAR(100),
 description VARCHAR(100),
 publie DATE,
 chemin VARCHAR(100),
 utilisateurs_id INT,
 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id)
);
CREATE TABLE statistique (
 id INT PRIMARY KEY,
 ajoute DATE,
 telechargements INT,
 livres_id INT NOT NULL,
 FOREIGN KEY (livres_id) REFERENCES livres (id)
);


INSERT INTO utilisateurs(id, nom, prenom, email)
VALUES(1, 'nom1', 'prenom1', '1email@gmail.com'),
(2, 'nom2', 'prenom2', '2email@gmail.ru'),
(3, 'nom3', 'prenom3', '3email@gmail.md'),
(4, 'nom4', 'prenom4', '4email@gmail.ro'),
(5, 'nom5', 'prenom5', '5email@gmail.com');
INSERT INTO livres(id, titre, auteur, categorie, description, publie, chemin,
utilisateurs_id)
VALUES(1, 'titre1', 'auteur1', 'categorie1', 'description1', '01.01.2000',
'chemin1', 1),
(2, 'titre2', 'auteur2', 'categorie2', 'description2', '02.01.2000',
'chemin2', 2),
 (3, 'titre3', 'auteur3', 'categorie3', 'description3', '03.01.2000',
'chemin3', 3),
 (4, 'titre4', 'auteur4', 'categorie4', 'description4', '04.01.2000',
'chemin4', 4),
 (5, 'titre5', 'auteur5', 'categorie5', 'description5', '05.01.2000',
'chemin5', 5);
INSERT INTO problemes(id, apparition, probleme, solution)
VALUES(1, '01.10.2000', 'probleme1', 'solution1'),
(2, '02.10.2000', 'probleme2', 'solution2'),
 (3, '03.10.2000', 'probleme3', 'solution3'),
 (4, '04.10.2000', 'probleme4', 'solution4'),
 (5, '05.10.2000', 'probleme5', 'solution5');
INSERT INTO problemes_utilisateurs(utilisateurs_id, problemes_id)
VALUES(1, 5), (2, 4), (3, 3), (4, 2), (5, 1);
INSERT INTO statistique(id, ajoute, telechargements, livres_id)
VALUES(1, '01.05.2000', 123, 1),
(2, '02.05.2000', 234, 2),
 (3, '03.05.2000', 345, 3),
 (4, '04.05.2000', 456, 4),
 (5, '05.05.2000', 567, 5);
